﻿using System;

namespace Calculator
{
    public class Math
    {
        // all the fields for the "Math" props
        private double _number1;
        private double _number2;
        private double _sum;
        private Operator _calculatorOperator;

        // the exceptions messages that get throwed when they need to
        private const string _notUseableDoubleValue = "this is not a useable double value.";
        private const string _nothingHaveBeenEntered = "there is no input at all.";


        /// <summary>
        /// An enum for the 4 types of operators, that can be used in this caculator
        /// </summary>
        public enum Operator
        {
            // +          -
            addition, subtraction,
            //   *             /
            multiplication, division
        }

        /// <summary>
        /// An property of the enum type "Operator", that will be needed to know which operator to use
        /// </summary>
        public Operator CalculatorOperator
        {
            get { return _calculatorOperator; }
            set { _calculatorOperator = value; }
        }
        /// <summary>
        /// the first number to be used for every caculation
        /// </summary>
        public double Number1
        {
            get { return _number1; }
            set { _number1 = value; }
        }
        /// <summary>
        /// the secound number to be used for every caculation
        /// </summary>
        public double Number2
        {
            get { return _number2; }
            set { _number2 = value; }
        }
        /// <summary>
        /// the end result after the calculation. 
        /// (Number1 will get the value of sum, after you are done with every caculation)
        /// </summary>
        public double Sum
        {
            get { return _sum;}
            set { _sum = Sum; }
        }
        /// <summary>
        /// constructor of the math class. set all the props to zero
        /// </summary>
        public Math()
        {
            Number1 = 0.0;
            Number2 = 0.0;
            Sum = 0.0;
        }

        /// <summary>
        /// this will call the methods needed, by using the enum, num1, num2 and sum.
        /// </summary>
        public void RunMath()
        {
            switch(CalculatorOperator)
            {
                case Operator.addition:
                    Addition();
                    break;
                case Operator.subtraction:
                    Subtraction();
                    break;
                case Operator.multiplication:
                    Multiplication();
                    break;
                case Operator.division:
                    Division();
                    break;
            }
        }
        /// <summary>
        /// takes a string and runs 2 checks on it. If it can be converted, it will be returned as a converted double
        /// </summary>
        /// <param name="input">the string input that needs to be converted</param>
        /// <returns></returns>
        public double ConvertStringToDouble(string input)
        {
            CheckString_IsItEmptyOrNull(input);
            return CheckStringForDoubleValue(input);
        }
        /// <summary>
        /// taking 2 numbers and adding them together
        /// </summary>
        public void Addition()
        {
            Sum = (Number1) + (Number2);
        }
        /// <summary>
        /// taking 1 number and subtracting it with the first one
        /// </summary>
        public void Subtraction()
        {
            Sum = (Number1) - (Number2);
        }
        /// <summary>
        /// taking 1 number and mulitiplire with the secound number
        /// </summary>
        public void Multiplication()
        {
            Sum = (Number1) * (Number2);
        }
        /// <summary>
        /// taking one number and dividing it with the secound number
        /// </summary>
        public void Division()
        {
            Sum = (Number1) / (Number2);
        }

        /// <summary>
        /// A way to reset the calculator
        /// </summary>
        public void Reset()
        {
            Number1 = 0.0;
            Number2 = 0.0;
            Sum = 0.0;
            // a way to kill or clear the enum "CalculatorOperator", but how?
        }

        /// <summary>
        /// check if the string can be converted to a double
        /// </summary>
        /// <param name="input">the string input that needs to be converted</param>
        /// <returns></returns>
        public double CheckStringForDoubleValue(string input)
        {
            double tempTestValue;
            Double.TryParse(input, out tempTestValue);

            if (tempTestValue != 0)
                return tempTestValue;
            else
                throw new ArgumentException(_notUseableDoubleValue);
        }
        /// <summary>
        /// checks if the input
        /// </summary>
        /// <param name="input"></param>
        public void CheckString_IsItEmptyOrNull(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
                throw new ArgumentNullException(_nothingHaveBeenEntered);
        }
    }
}
